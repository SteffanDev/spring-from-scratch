package com.steffan.springFromScratch.security;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class AppProperties {

    @Autowired
    Environment env;

    //using a property file to get the token secret
    public String getTokenSecret(){
        return env.getProperty("tokenSecret");
    }
}

package com.steffan.springFromScratch.io.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

@Entity(name="users")
public class UserEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private long id;

    @Column(nullable=false)
    private String userId;
    @Column(nullable=false,length=50)
    private String firstName;
    @Column(nullable=false,length=50)
    private String lastName;
    @Column(nullable=false,length=120,unique=true)
    private String email;
    @Column(nullable=false)
    private String encryptedPassword;

    private String emailVerificationToken;
    @Column(nullable=false)
    private Boolean emailVerificationStatus = false;
}

package com.steffan.springFromScratch.ui.service;

import com.steffan.springFromScratch.shared.dto.UserDto;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {

    UserDto createUser(UserDto userDto);
    UserDto getUser(String email);
    UserDto getUserById(String id);
}
